# WeatherForecast

I started this task during the weekend, despite the fact the recruiter said asking if any question came up.

But beforehand, I got an advice, not to overthink the task and because the objective said the implementation details is up to me, I convicted with the following assumptions:
- The already represented 'WeatherForecast' model is sufficient for the task, just add an Id column for Db persistency reason.
- For simplicity, the app just accept the exact structure with header, of course, errors are handled if the uploaded file not meet these requirements.
	
After that, I wrote some main point for the development:
- Db persist
- CSV scheme -> migration
  - CSV parser
  - (measure)
  - Test
- UI
	- Button
	- Dialog -> Error
	- Table
- (Paging)

I forked the given github repository, and for a while I worked with that, when I realized the forked one is public and unable to change, so create a brand new repo and moved there my already committed changes and continue to work from there.

The majority of my time is spent to play with angular, because literally I have no actual dev experience with any javascript-based frontend framework, and this is reason why I started my tasks with the frontend part, I would like to started with the hard part.

About the db related part, just searched a simple repository code on the net and copied into the code, after the db context part just created a migration from dev console.

Created an individual service for the CSV Parser and handled several error may came up. Wrote some test for this service.

I created an upload component, I registered into the app.modules and add it to the navbar.
On this component, if we clicked on the button, then a filedialog opens to choose a 'csv' file.

If it's fulfill the requirements, then a table appears with the currently uploaded values.
If the process fails, an error message appears on the page.

About the stretch goals:
- I started to use some virtualized table to optimize rendering lots of data, but some pagination feature would be more elegant to represent the desired data.
- For CSV processing, I tried to work with it asynchronously to keep in mind the amount of data could be huge, some measuring also would be great how it's performed.
- SQL connection may came from connection pool, that also could be an improvement point.

Final words: I really enjoyed to learnt some basic about Angular with this task and of course I course spent more than an our for this task, but worth it, even it not gonna be successful.
