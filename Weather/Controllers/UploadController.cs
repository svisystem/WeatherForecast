﻿using CsvHelper;
using CsvHelper.TypeConversion;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Weather.Models;
using Weather.Repository;
using Weather.Services;

namespace Weather.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class UploadController : ControllerBase
    {
        private readonly IWeatherInformationParser _parser;
        private readonly IRepositoryBase<WeatherForecast> _forecastRepository;
        private readonly ILogger<UploadController> _logger;

        public UploadController(IWeatherInformationParser parser,
            IRepositoryBase<WeatherForecast> forecastRepository,
            ILogger<UploadController> logger)
        {
            _parser = parser;
            _forecastRepository = forecastRepository;
            _logger = logger;
        }

        [HttpPost]
        public async Task<IActionResult> Post()
        {
            var request = Request;
            using var stream = new StreamReader(request.Body);
            var body = await stream.ReadToEndAsync();

            try
            {
                var parsedData = _parser.ParseInputAsync<WeatherForecast>(body);

                var response = await PersistForecastData(parsedData).ToListAsync();

                return Ok(response);
            }
            catch (HeaderValidationException ex)
            {
                _logger.LogInformation(ex, ex.Message, body);
                return BadRequest("The header is not presented in the requested file.");
            }
            catch (TypeConverterException ex)
            {
                _logger.LogInformation(ex, ex.Message, body);
                return BadRequest("The requested file is not wellformed.");

            }
            catch(Exception ex)
            {
                _logger.LogInformation(ex, ex.Message, body);
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        private async IAsyncEnumerable<WeatherForecast> PersistForecastData(IAsyncEnumerable<WeatherForecast> input)
        {
            await foreach (var forecastRecort in input)
            {
                yield return await _forecastRepository.AddAsync(forecastRecort);
            }
        }
    }
}
