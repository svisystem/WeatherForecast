﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using Weather.Models;
using Weather.Repository;

namespace Weather.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IRepositoryBase<WeatherForecast> _forecastRepository;

        public WeatherForecastController(ILogger<WeatherForecastController> logger,
            IRepositoryBase<WeatherForecast> forecastRepository)
        {
            _logger = logger;
            _forecastRepository = forecastRepository;
        }

        [HttpGet]
        public IEnumerable<WeatherForecast> Get()
        {
            return _forecastRepository.GetAll();
        }
    }
}
