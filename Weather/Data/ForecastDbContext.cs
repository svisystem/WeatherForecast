﻿using Microsoft.EntityFrameworkCore;
using Weather.Models;

namespace Weather.Data
{
    public class ForecastDbContext : DbContext
    {
        public DbSet<WeatherForecast> Forecasts { get; set; }

        public ForecastDbContext(DbContextOptions<ForecastDbContext> options)
            : base(options)
        {
            Database.EnsureCreated();
        }
    }
}
