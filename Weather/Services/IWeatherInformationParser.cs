﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Weather.Services
{
    public interface IWeatherInformationParser
    {
        IAsyncEnumerable<T> ParseInputAsync<T>(string raw);
    }
}