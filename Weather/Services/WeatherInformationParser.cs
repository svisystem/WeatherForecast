﻿using CsvHelper;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace Weather.Services
{
    public class WeatherInformationParser : IWeatherInformationParser
    {
        public async IAsyncEnumerable<T> ParseInputAsync<T>(string raw)
        {
            using var stringReader = new StringReader(raw);

            using (var csvReader = new CsvReader(stringReader, CultureInfo.InvariantCulture))
            {
                await foreach(var item in csvReader.GetRecordsAsync<T>())
                {
                    yield return item;
                }
            }
        }
    }
}
