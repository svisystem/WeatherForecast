import { NgModule } from '@angular/core';
import { ServerModule } from '@angular/platform-server';
import { AppComponent } from './app.component';
import { AppModule } from './app.module';
import { ScrollingModule } from '@angular/cdk/scrolling';

@NgModule({
  imports: [AppModule, ServerModule, ScrollingModule],
    bootstrap: [AppComponent]
})
export class AppServerModule { }
