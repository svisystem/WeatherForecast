import { Component, Inject } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { IWeatherForecast } from '../interfaces/iWeatherForecast'
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { retry, catchError, throwError } from 'rxjs';

@Component({
  selector: 'app-upload-data',
  templateUrl: './upload-data.component.html',
  styleUrls: ['./upload-data-component.css']
})
export class UploadDataComponent {
  public forecasts: IWeatherForecast[];
  itemSize = 1;
  private _baseUrl: string;
  public errorMessage: string;

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string, public dialog: MatDialog) {
    this._baseUrl = baseUrl;
  }

  onFileSelected(event) {
    this.forecasts = null;
    this.errorMessage = null;
    const file: File = event.target.files[0];
    if (file) {
        this.http.post(this._baseUrl + 'upload', file, ).subscribe(result =>
          this.forecasts = result as IWeatherForecast[], error => {
            let errMsg = error.error ? error.error : error.message ? `${error.status} - ${error.statusText}` : 'Server error';
            this.errorMessage = errMsg
          });      
    }
  }
}
