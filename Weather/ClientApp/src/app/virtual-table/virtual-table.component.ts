import { Component, Input } from '@angular/core';
import { IWeatherForecast } from '../interfaces/iWeatherForecast'

@Component({
  selector: 'app-virtual-table',
  templateUrl: './virtual-table.component.html',
  styleUrls: ['./virtual-table-component.css']
})
export class VirtualTableComponent {
  @Input() items: IWeatherForecast[];
  @Input() itemSize;

  constructor() {  }
}

