using CsvHelper.Configuration.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Weather.Models
{
    [Table("Forecasts")]
    public class WeatherForecast
    {
        [Key]
        [Optional]
        public int Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public double TemperatureC { get; set; }

        [Required]
        public double TemperatureF => 32 + (TemperatureC / 0.5556);

        [Required]
        public string Summary { get; set; }
    }
}
