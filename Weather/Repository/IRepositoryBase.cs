﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Weather.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class, new()
    {
        IEnumerable<TEntity> GetAll();

        Task<TEntity> AddAsync(TEntity entity);

        Task<TEntity> UpdateAsync(TEntity entity);
    }
}
