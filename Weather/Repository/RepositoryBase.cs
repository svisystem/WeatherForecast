﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Weather.Data;

namespace Weather.Repository
{
    public class RepositoryBase<TEntity> : IRepositoryBase<TEntity> where TEntity : class, new()
    {
        protected readonly ForecastDbContext ForecastDbContext;

        public RepositoryBase(ForecastDbContext repositoryPatternDemoContext)
        {
            ForecastDbContext = repositoryPatternDemoContext;
        }

        public IEnumerable<TEntity> GetAll()
        {
            try
            {
                return ForecastDbContext.Set<TEntity>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await ForecastDbContext.AddAsync(entity);
                await ForecastDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<TEntity> UpdateAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                ForecastDbContext.Update(entity);
                await ForecastDbContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated: {ex.Message}");
            }
        }
    }
}