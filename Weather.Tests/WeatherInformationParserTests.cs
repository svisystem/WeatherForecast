using FluentAssertions;
using NUnit.Framework;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Weather.Models;
using Weather.Services;
using CsvHelper;
using CsvHelper.TypeConversion;

namespace Weather.Tests
{
    public class WeatherInformationParserTests
    {
        [Test]
        public async Task TheInputWellFormed_WithoutHeader()
        {
            var input = $"11/15/2021 11:36:27 AM,-13,9,Chilly{Environment.NewLine}" +
                        $"11/16/2021 11:36:27 AM,44,111,Chilly{Environment.NewLine}" +
                        $"11/17/2021 11:36:27 AM,-20,-3,Sweltering{Environment.NewLine}" +
                        $"11/18/2021 11:36:27 AM,-10,15,Hot{ Environment.NewLine}" +
                        $"11/19/2021 11:36:27 AM,-18,0,Scorching{ Environment.NewLine}" +
                        $"11/20/2021 11:36:27 AM,-18,0,Cool";

            var parser = new WeatherInformationParser();

            var result = parser.ParseInputAsync<WeatherForecast>(input);

            await result.Invoking(async r => await r.ToListAsync()).Should().ThrowAsync<HeaderValidationException>();
        }

        [Test]
        public async Task TheInputWellFormed_WithtHeader()
        {
            var input = $"Date,TemperatureC,TemperatureF,Summary{Environment.NewLine}" +
                        $"11/15/2021 11:36:27 AM,-13,9,Chilly{Environment.NewLine}" +
                        $"11/16/2021 11:36:27 AM,44,111,Chilly{Environment.NewLine}" +
                        $"11/17/2021 11:36:27 AM,-20,-3,Sweltering{Environment.NewLine}" +
                        $"11/18/2021 11:36:27 AM,-10,15,Hot{ Environment.NewLine}" +
                        $"11/19/2021 11:36:27 AM,-18,0,Scorching{ Environment.NewLine}" +
                        $"11/20/2021 11:36:27 AM,-18,0,Cool";

            var parser = new WeatherInformationParser();

            var result = await parser.ParseInputAsync<WeatherForecast>(input).ToListAsync();

            result.Should().BeOfType<List<WeatherForecast>>();
            result.Count.Should().BeGreaterThan(0);
        }

        [Test]
        public async Task TheInputNotWellFormed()
        {
            var input = $"Date,TemperatureC,TemperatureF,Summary{Environment.NewLine}" +
                        $"11/15/2021 11:36:27 AM,-13,9,Chilly{Environment.NewLine}" +
                        $"11/16/2021 11:36:27 AM,44,111,Chilly{Environment.NewLine}" +
                        $"11/17/2021 11:36:27 AM, whatif,-3,Sweltering, 11/17/2021 11:36:27 AM,-20,-3,Sweltering{Environment.NewLine}" +
                        $"11/18/2021 11:36:27 AM,-10,15,Hot{ Environment.NewLine}" +
                        $"11/19/2021 11:36:27 AM,-18,0,Scorching{ Environment.NewLine}" +
                        $"11/20/2021 11:36:27 AM,-18,0,Cool";

            var parser = new WeatherInformationParser();

            var result = parser.ParseInputAsync<WeatherForecast>(input);

            await result.Invoking(async r => await r.ToListAsync()).Should().ThrowAsync<TypeConverterException>();
        }
    }
}